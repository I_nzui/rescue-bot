// Libraries
#include <ESP8266WiFi.h>
#include "Webpage.h"

// RescueBot Mode
bool autonoom = 0;  // 0 == zelfrijdend, 1 == wordt bediend

// Timers
static unsigned long obstakelTime = millis();
static unsigned long obstakelCheckTime = millis();
static unsigned long afgrondTime = millis();
static unsigned long afgrondCheckTime = millis();
static unsigned long slachtofferTime = millis();
static unsigned long waterTime = millis();
static unsigned long waterVoorTime = millis();
static unsigned long omdraaiTime = millis();

// Externe Variabelen
extern const char* ssid;
extern const char* passwd;

// Variabelen
bool autoStop = 1;
bool rijBlock = 0;
bool obstakel = 0;
bool afgrond = 0;
bool afgrondReset = 0;
bool obstakelReset = 0;
bool slachtoffer = 0;
bool slachtofferReset = 1;
bool waterLinks = 0;
bool waterRechts = 0;
bool waterLinksCheck = 0;
bool waterRechtsCheck = 0;
bool waterVoorCheck = 0;
bool waterVoor = 0;
bool laatsteWaterKant = 0;
bool draaiKant = 0;
int omdraai = 0;
    
void setup() {
  Serial.begin(9600);
  pinMode(reed, INPUT);
  pinMode(echoVoor, INPUT);
  pinMode(triggerVoor, OUTPUT);
  pinMode(echoOnder, INPUT);
  pinMode(triggerOnder, OUTPUT); 
  pinMode(irLinks, INPUT);
  pinMode(irRechts, INPUT);
  pinMode(motorLinks1, OUTPUT);
  pinMode(motorLinks2, OUTPUT);
  pinMode(motorRechts1, OUTPUT);
  pinMode(motorRechts2, OUTPUT);
  WiFi.softAP(ssid, passwd);
  server.begin();

  // Autonoom Setup
  if (!autonoom){
    delay(5000);
    Drive(1);
  }
}

void loop() {
  webPagina();
  if (!autonoom) {  // Autonoom
    checkVoorObstakels();
    if (!slachtoffer){
      if (obstakel){
        if ((millis() - obstakelTime  < 500)){
          Drive(2);
        }
        if ((millis() - obstakelTime  > 500) && (millis() - obstakelTime  < 1200)){
          Drive(3);
        }
        if ((millis() - obstakelTime  > 1200) && (millis() - obstakelTime  < 3000)){
          Drive(1);
        }
        if ((millis() - obstakelTime  > 3000) && (millis() - obstakelTime  < 3750)){
          Drive(4);
        }
        if (millis() - obstakelTime  > 3750){
          Drive(1);
          obstakel = 0;
        }
      }

      if (afgrond){
        if ((millis() - afgrondTime  < 500)){
          Drive(2);
        }
        if ((millis() - afgrondTime  > 500) && (millis() - afgrondTime  < 1900)){
          Drive(4);
        }
        if(millis() - afgrondTime > 1900){
          Drive(1);
          afgrond = 0;
        }
      }
      
      if (waterLinks){
        if (millis() - waterTime  < 300){
          Drive(4);
        }
        else{
          Drive(1);
          waterLinks = 0;
        }
      }

      if (waterRechts){
        if (millis() - waterTime  < 300){
          Drive(3);
        }
        else{
          Drive(1);
          waterRechts = 0;
        }
      }

      if (omdraai == 1){
        if (millis() - omdraaiTime < 200){
          Drive(2);
        }
        if ((millis() - omdraaiTime > 200) && (millis() - omdraaiTime < 1700)){
          if (draaiKant){
            Drive(4);
          }
          else{
            Drive(3);
          }
        }
        if ((millis() - omdraaiTime > 1700) && (millis() - omdraaiTime < 1750)){
          Drive(1);
        }
        if (millis() - omdraaiTime > 6200){
          omdraai = 0;
        }
      }

      if (omdraai == 2){
        if (millis() - omdraaiTime < 200){
          Drive(2);
        }
        if ((millis() - omdraaiTime > 200) && (millis() - omdraaiTime < 3450)){
          Drive(4);
        }
        if (millis() - omdraaiTime > 3450){
          Drive(1);
          omdraai = 0;
        }
      }
    }

    else{
      Drive(0);
      if (digitalRead(reed)){
        if (slachtofferReset){
          slachtofferTime = millis();
          slachtofferReset = 0;
        }
        if ((millis() - slachtofferTime  > 5000)){
          Drive(1);
          slachtoffer = 0;
          slachtofferReset = 1;
        }
      }
    }
  }

  else{                 // Bediening
    if (autoStop == 1) {
      if ((afgrondCheck()) == 1){
        Drive(2);
        rijBlock = 1;
        delay(500);
        Drive(0);
      }
      if ((obstakelCheck()) == 1){
        Drive(2);
        delay(500);
        Drive(0);
      }
    }
  }
}

void Drive(int direction){
  if (direction == 0) { // Stop Motor
    digitalWrite(motorLinks1, 0);
    digitalWrite(motorLinks2, 0);
    digitalWrite(motorRechts1, 0);
    digitalWrite(motorRechts2, 0);
  }
  if (rijBlock != 1) {
    if (direction == 1) { // Rij naar voren
      digitalWrite(motorLinks1, 1);
      digitalWrite(motorLinks2, 0);
      digitalWrite(motorRechts1, 1);
      digitalWrite(motorRechts2, 0);
    }
    if (direction == 2) { // Rij naar achteren
      digitalWrite(motorLinks1, 0);
      digitalWrite(motorLinks2, 1);
      digitalWrite(motorRechts1, 0);
      digitalWrite(motorRechts2, 1);
    }
    if (direction == 3) { // Rij naar links
      digitalWrite(motorLinks1, 0);
      digitalWrite(motorLinks2, 1);
      digitalWrite(motorRechts1, 1);
      digitalWrite(motorRechts2, 0);
    }
    if (direction == 4) { // Rij naar rechts
      digitalWrite(motorLinks1, 1);
      digitalWrite(motorLinks2, 0);
      digitalWrite(motorRechts1, 0);
      digitalWrite(motorRechts2, 1);
    }
  }
}

void checkVoorObstakels(){
  if (obstakelCheck()){
    if (!obstakel){
      obstakel = 1;
      obstakelTime = millis();
    }
  }
  if (afgrondCheck()){
    if (!afgrond){
      afgrond = 1;
      afgrondTime = millis();
    }
  }
  if (!digitalRead(reed)){
    if (!slachtoffer){
      slachtoffer = 1;
    }
  }
  if (digitalRead(irLinks)){
    if (!waterLinksCheck){
      laatsteWaterKant = 0;
      obstakel = 0;
      waterLinksCheck = 1;
      if (!waterRechtsCheck && !waterVoorCheck){
        waterVoorCheck = 1;
        waterVoorTime = millis();
      }
    }
  }
  if (digitalRead(irRechts)){
    if (!waterRechtsCheck){
      laatsteWaterKant = 1;
      obstakel = 0;
      waterRechtsCheck = 1;
      if (!waterLinksCheck && !waterVoorCheck){
        waterVoorCheck = 1;
        waterVoorTime = millis();
      }
    }
  }
  if (waterVoorCheck){
    if ((waterLinksCheck) && (waterRechtsCheck)){
      if (omdraai == 0){
        draaiKant = laatsteWaterKant;
        omdraai = 1;
        omdraaiTime = millis(); 
      }
      if ((omdraai == 1) && (millis() - omdraaiTime > 2000) && (millis() - omdraaiTime < 6200)){
        omdraai = 2;
        omdraaiTime = millis();
      }
      waterVoorCheck = 0;
      waterLinksCheck = 0;
      waterRechtsCheck = 0;
    }
    else if (millis() - waterVoorTime > 250){
      waterTime = millis();
      if (waterLinksCheck){
        waterLinks = 1;
        waterLinksCheck = 0;
        waterVoorCheck = 0;
      }
      else{
        waterRechts = 1;
        waterRechtsCheck = 0;
        waterVoorCheck = 0;
      }
      waterVoorCheck = 0;
    }
  }
}
  
bool obstakelCheck(){
  digitalWrite(triggerVoor, LOW);
  delayMicroseconds(2);
  digitalWrite(triggerVoor, HIGH);
  delayMicroseconds(10);
  digitalWrite(triggerVoor, LOW);
  if (pulseIn(echoVoor, HIGH, 500) == 0) {
    obstakelReset = 0;
    return(0);
  }
  else{
    if (!obstakelReset){
      obstakelReset = 1;
      obstakelCheckTime = millis();
    }
    if ((obstakelReset) && (millis() - obstakelCheckTime > 200)){
      obstakelReset = 0;
      return(1);
    }
    else{
      return(0);
    }
  }
}

bool afgrondCheck(){
  digitalWrite(triggerOnder, LOW);
  delayMicroseconds(2);
  digitalWrite(triggerOnder, HIGH);
  delayMicroseconds(10);
  digitalWrite(triggerOnder, LOW);
  if (pulseIn(echoOnder, HIGH, 1000) != 0) {
    afgrondReset = 0;
    return(0);
  }
  else{
    if (!afgrondReset){
      afgrondReset = 1;
      afgrondCheckTime = millis();
    }
    if ((afgrondReset) && (millis() - afgrondCheckTime > 100)){
      afgrondReset = 0;
      return(1);
    }
    else{
      return(0);
    }
  }
}
