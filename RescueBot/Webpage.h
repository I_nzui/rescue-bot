// IP ESP: 192.168.4.1

// Define componenten
#define reed 2
#define echoOnder 16
#define triggerOnder 12
#define echoVoor 5
#define triggerVoor 1
#define irLinks 14
#define irRechts 3
#define motorLinks1 4
#define motorLinks2 0
#define motorRechts1 15
#define motorRechts2 13

// Externe Functions
extern void Drive(int direction);

// Externe Variabelen
extern bool autonoom;
extern bool autoStop;
extern bool rijBlock;

// Variabelen
const char* ssid = "Botty-McBotface";
const char* passwd = "ThotQueen";
WiFiServer server(80);
String header;
unsigned long currentTime = millis();
unsigned long previousTime = 0;
const long timeoutTime = 2000;

void webPagina(){
  WiFiClient client = server.available();
  if (client) {
    String currentLine = "";
    currentTime = millis();
    previousTime = currentTime;
    while (client.connected() && currentTime - previousTime <= timeoutTime) {
      currentTime = millis();
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        header += c;
        if (c == '\n') {
          if (currentLine.length() == 0) {
            if (header.indexOf("GET /Voorwaarts") >= 0) {
              Drive(1);
            }
            if (header.indexOf("GET /Stop") >= 0) {
              Drive(0);
            }
            if (header.indexOf("GET /Links") >= 0) {
              Drive(3);
            }
            if (header.indexOf("GET /Rechts") >= 0) {
              Drive(4);
            }
            if (header.indexOf("GET /Achteren") >= 0) {
              Drive(2);
            }
            if (header.indexOf("GET /autoStopAan") >= 0) {
              autoStop = 1;
            }
            if (header.indexOf("GET /autoStopUit") >= 0) {
              autoStop = 0;
            }
            if (header.indexOf("GET /rijBlockUit") >= 0) {
              rijBlock = 0;
            }
            if (header.indexOf("GET /veranderMode") >= 0) {
              if (autonoom){
                autonoom = 0;
                Drive(1);
                rijBlock = 0;
              }
              else{
                autonoom = 1;
                Drive(0);
              }
            }
            
            // Web Pagina
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println(".knopGroen { background-color: #71e300; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".knopRood { background-color: #e30000; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".knopMidden { background-color: #195B6A; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".knopLinks { background-color: #195B6A; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".knopRechts { background-color: #195B6A; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}</style></head>");
            
            client.println("<body><h1>RescueBot Web Control</h1>");
            if (!autonoom){
              client.println("<h1>Auto Mode</h1>");
            }
            client.println("<b>Ian Zuiderent & Yoeri Maanster</b>");
            if (autonoom){
              client.println("<p><a href=\"/Voorwaarts\"><button class=\"knopMidden\">^</button></a></p>");
              client.println("<p><a href=\"/Links\"><button class=\"knopLinks\"><</button></a> <a href=\"/Stop\"><button class=\"knopMidden\">STOP</button></a> <a href=\"/Rechts\"><button class=\"knopRechts\">></button></a></p>");
              client.println("<p><a href=\"/Achteren\"><button class=\"knopMidden\">v</button></a></p>");
              if (!autoStop) {
                client.println("<p><a href=\"/autoStopAan\"><button class=\"knopRood\">AutoStop disabled!</button></a></p>");
              }
              else{
                client.println("<p><a href=\"/autoStopUit\"><button class=\"knopGroen\">AutoStop enabled</button></a></p>");
              }
              if (!rijBlock){
                client.println("<p><a href=\"/\"><button class=\"knopMidden\">Fall Block Disabled</button></a></p>");
              }
              else{
                client.println("<p><a href=\"/rijBlockUit\"><button class=\"knopRood\">Remove Fall Block!</button></a></p>");
              }
            }
            client.println("<p><a href=\"/veranderMode\"><button class=\"knopMidden\">Change mode</button></a></p>");
            client.println("</body></html>");
            client.println();
            break;
            } 
            else {
              currentLine = "";
            }
        } 
        else if (c != '\r') {
          currentLine += c;
        }
      }
    }
    header = "";
    client.stop();
  }
}
